package compilatore.lexical;

import java_cup.runtime.*;
import compilatore.parser.sym;
%%

%cup
%public
%class ScanText
//conta le righe
%line
%unicode


%{
  //Codice copiato testualmente nella classe generata.
  StringBuffer string = new StringBuffer();
  private ComplexSymbolFactory sf;
  public ScanText(java.io.Reader in, ComplexSymbolFactory sf) {
    this(in);
    this.sf = sf;
  }

  public int currentLineNumber(){
    return yyline + 1;
  }
%}

/* main character classes */
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
Spaziatura = [ \t\f]
WhiteSpace = {LineTerminator} | [ \t\f]

/* comments */
Comment = "//" {InputCharacter}*

/* identifiers */
Identifier = [:jletter:][:jletterdigit:]*

/* string and character literals */
StringCharacter = [^\r\n\"\\]

/* integer literals */
DecIntegerLiteral = [:digit:]+

HexIntegerLiteral = 0x 0* {HexDigit} {1,8}
HexDigit          = [0-9a-fA-F]

%state STRING

%init{
  //codice copiato testualmente nel costruttore:

  //messaggio iniziale
  //System.out.println("\033[0;31mInizio analisi lessicale\033[0m");

%init}

%eof{
  //codice eseguito quando viene raggiunta la fine del file
  //System.out.println("\033[0;31mFine analisi lessicale\033[0m");
%eof}

%eofval{
    return sf.newSymbol("EOF",sym.EOF);
%eofval}

%%

<YYINITIAL> {

  /* keywords */
  "endloop"                   {return sf.newSymbol("ENDLOOP",sym.ENDLOOP);}
  "input"                     {return sf.newSymbol("INPUT",sym.INPUT);}
  "loop"                      {return sf.newSymbol("BEGINLOOP",sym.BEGINLOOP);}
  "newLine"                   {return sf.newSymbol("NEWLINE",sym.NEWLINE);}
  "output"                    {return sf.newSymbol("OUTPUT",sym.OUTPUT);}
  "declare"                   {return sf.newSymbol("DECLARE",sym.DECLARE);}
  "as"                        {return sf.newSymbol("AS",sym.AS);}
  "sizeof"                    {return sf.newSymbol("Size of a variable",sym.SIZEOF);}
  "true"                      {return sf.newSymbol("True",sym.TRUE);}
  "false"                     {return sf.newSymbol("False",sym.FALSE);}
  "if"                        {return sf.newSymbol("If",sym.IF);}  
  "else"                      {return sf.newSymbol("Else",sym.ELSE);}
  "endif"                     {return sf.newSymbol("Else",sym.ENDIF);}

  /* type */
  "int"                       {return sf.newSymbol("AS",sym.INT);}
  "array"                     {return sf.newSymbol("ARRAY",sym.ARRAY);}
 
  /* separators */
  "("                            { return sf.newSymbol("LPAREN",sym.LPAREN);}
  ")"                            { return sf.newSymbol("RPAREN",sym.RPAREN);}
  "["                            { return sf.newSymbol("LQUADRA",sym.LQUADRA);}
  "]"                            { return sf.newSymbol("RQUADRA",sym.RQUADRA);}
  
  /* operators */
  "="                            { return sf.newSymbol("UG",sym.UG);}
  "+"                            { return sf.newSymbol("PIU",sym.PIU);}
  "-"                            { return sf.newSymbol("MENO",sym.MENO);}
  "/"                            { return sf.newSymbol("DIV",sym.DIV);}
  "*"                            { return sf.newSymbol("PER",sym.PER);}
  "%"                            { return sf.newSymbol("MOD",sym.MOD);}
  ":"                            { return sf.newSymbol("DUE_PUNTI",sym.DUE_PUNTI);}
  "?"                            { return sf.newSymbol("PUNTO_INTERROGATIVO",sym.PUNTO_INTERROGATIVO);}
  "<"                            { return sf.newSymbol("Less than",sym.LT);}           
  "<="                           { return sf.newSymbol("Less than or equal to",sym.LE);}
  "=="                           { return sf.newSymbol("Equal",sym.EQ);}
  "!="                           { return sf.newSymbol("Not equal",sym.NE);}
  ">"                            { return sf.newSymbol("Greater than",sym.GT);}
  ">="                           { return sf.newSymbol("Greater than or equal to",sym.GE);}
  "and"                          { return sf.newSymbol("And",sym.AND);}
  "or"                           { return sf.newSymbol("Or",sym.OR);}
  
  /* string literal */
  \"                             { yybegin(STRING); string.setLength(0); }

  {LineTerminator}+               {return sf.newSymbol("CR",sym.CR);}

  /* comments */
  {Comment}      { /* ignore */ }

  /* whitespace */
  {WhiteSpace}                   { /* ignore */ }

  "&"{Spaziatura}*{Comment}*{LineTerminator}    {/* ignore */ }

  /* identifiers */ 
  {Identifier}                   {return sf.newSymbol("IDENT", sym.IDENT, yytext());}  

  /* numbers */
  {DecIntegerLiteral}            {return sf.newSymbol("NUMERO", sym.NUMERO, new Integer(yytext()));}
  {HexIntegerLiteral}            {return sf.newSymbol("NUMERO", sym.NUMERO, Integer.decode(yytext()));}

  .                              {return sf.newSymbol("ALTRO", sym.ALTRO, new Integer(yytext()));}
}

<STRING> {
  \"                             { yybegin(YYINITIAL);return sf.newSymbol("STRINGA", sym.STRINGA, string.toString()); }
  
  {StringCharacter}+             { string.append( yytext() ); }
  
  /* escape sequences */
  "\\b"                          { string.append( '\b' ); }
  "\\t"                          { string.append( '\t' ); }
  "\\n"                          { string.append( '\n' ); }
  "\\f"                          { string.append( '\f' ); }
  "\\r"                          { string.append( '\r' ); }
  "\\\""                         { string.append( '\"' ); }
  "\\'"                          { string.append( '\'' ); }
  "\\\\"                         { string.append( '\\' ); }
  /* error cases */
  \\.                            { throw new RuntimeException("Illegal escape sequence \""+yytext()+"\""); }
  {LineTerminator}               { throw new RuntimeException("Unterminated string at end of line"); }
}

