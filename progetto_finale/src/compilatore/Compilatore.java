package compilatore;

import java_cup.runtime.*;
import java.io.*;

import lt.macchina.*;
import static lt.macchina.Macchina.*;
import compilatore.lexical.*;
import compilatore.parser.*;
import generatore.*;
import generatore.istr.*;

public class Compilatore {
  public static void main(String[] args) throws IOException{

        if(args.length < 1){
          throw new IllegalArgumentException("I parametri da inserire sono SOURCE DESTINATION");
        }
        String source = args[0];
        String dest="eseguibile";
        if(args.length==2){
          dest=args[1];
        }
    
        //creazione della symbol factory
        ComplexSymbolFactory sf = new ComplexSymbolFactory();
    
        //creazione dell'analizzatore lessicale
        File f = new File(source);
        ScanText scanner = new ScanText(new FileReader(f), sf);
        Context.setFile(f);
            
        //creazione del parser
        Parser p = new Parser(scanner, sf);
    
        try {
          System.out.println("START COMPILING");
          Symbol ris = p.parse();
          Context.resetFunction();
          System.out.println("Parsing Tree produced");
          Prog risultato = (Prog) ris.value;   
    
          SymbolTable table = risultato.getSymbolTable();
          System.out.println("Symbol table computed");
          
          Codice c = new Codice(dest);
          /* 
          PRIMA PARTE: calcola indirizzi variabili e aggiorna i descrittori 
          */
          int nextAddr = 0;
          for (Descriptor d : table){
            nextAddr = d.calculateAddress(nextAddr).getNext();
          }
          //sposta SP di tante caselle quante sono le variabili+registri
          c.genera(PUSHIMM,nextAddr);
          c.genera(MOVESP);

          risultato.generaCodice(c);

          c.genera(HALT);

          /* Completa le istruzioni assegnando gli indirizzi */
          table.stream().forEach(e -> e.updatePointers(c));
          System.out.println(table);
          c.fineCodice();
    
        } catch (Exception e)  {
          System.out.println("\n\033[4;31mCompilazione non andata a buon fine\033[0m");
          e.printStackTrace();
        }
    
  }
}
