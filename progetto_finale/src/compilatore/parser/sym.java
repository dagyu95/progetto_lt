
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20140611 (SVN rev 31)
// Thu Jun 21 11:13:54 CEST 2018
//----------------------------------------------------

package compilatore.parser;

/** CUP generated interface containing symbol constants. */
public interface sym {
  /* terminals */
  public static final int PIU = 2;
  public static final int NUMERO = 39;
  public static final int UNARIO = 18;
  public static final int ALTRO = 42;
  public static final int LQUADRA = 22;
  public static final int GE = 13;
  public static final int OUTPUT = 26;
  public static final int RQUADRA = 21;
  public static final int INPUT = 25;
  public static final int LPAREN = 19;
  public static final int STRINGA = 41;
  public static final int INT = 23;
  public static final int ARRAY = 24;
  public static final int ENDLOOP = 29;
  public static final int CR = 35;
  public static final int RPAREN = 20;
  public static final int AND = 16;
  public static final int SIZEOF = 34;
  public static final int LT = 10;
  public static final int MENO = 3;
  public static final int PER = 5;
  public static final int OR = 17;
  public static final int DIV = 4;
  public static final int BEGINLOOP = 28;
  public static final int IF = 30;
  public static final int LE = 11;
  public static final int EOF = 0;
  public static final int UG = 9;
  public static final int TRUE = 36;
  public static final int error = 1;
  public static final int NEWLINE = 27;
  public static final int MOD = 6;
  public static final int IDENT = 40;
  public static final int DUE_PUNTI = 7;
  public static final int EQ = 14;
  public static final int DECLARE = 32;
  public static final int ENDIF = 38;
  public static final int ELSE = 31;
  public static final int NE = 15;
  public static final int FALSE = 37;
  public static final int AS = 33;
  public static final int PUNTO_INTERROGATIVO = 8;
  public static final int GT = 12;
}

