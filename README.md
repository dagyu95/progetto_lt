# Progetto Linguaggi e Traduttori AA 2017/2018
**Studenti**: Gaetano D'Agostino 869878 & Samuele Maria Chiesa 871173
## Caratteristiche aggiuntive
Alle specifiche del progetto iniziale sono state aggiunte le seguente caratteristiche:

* dichiarazioni obbligatorie per ogni variabile
* tipo di dato array
* struttura blocco if-then-else


Nella directory `progetto_finale/examples/` sono contenuti alcuni esempi di codice compilabile. 

### Esempio1 (Dichiarazioni)
Per esempio il seguente sorgente:
```c
declare y as int
y=x+2
output y
```
darà un errore in compilazione dicendo che la variabille `x` non è stata dichiarata

### Esempio2 (Uso array)

La dimensione dell'array viene decisa staticamente durante la dichiarazione e la modifica o accesso ad un cella di un array viene fatta usando una sintassi simile al C:
```c
declare a as array 10
a[0] = 10
output 30*a[0]
```
P.S. come in C non vi alcun controllo sull'accesso fuori dai margini dell'array

### Esempio3 (Uso if-then-else)
Nel seguente codice se viene inserito un numero >0 allora viene scelto il ramo dell'if altrimenti se viene inserito un numero =0 viene scelto il ramo dell'else.
```c
if input "scegli ramo: "
  output "branch 1"
else 
  output "branch 2"
endif
```
## Compilazione
Per compilare si può digitare il seguente comando:
```
./lt -cp "progetto_finale/dist/*" progetto_finale/examples/selectionsort.lt
```
Il file `lt` è eseguibile solo se si ha nella propria macchina un interprete Bash. Altrimenti si devono compilare nel seguente modo:
```
cd progetto_finale/examples
java -cp "../dist/*" compilatore.Compilatore selectionsort.lt nome_eseguibile
```
## Esecuzione
Per eseguire, analogamente, si può digitare il comando:
```
./lt  -ep "progetto_finale/dist/*" eseguibile
```
oppure:
```
java -cp "../dist/*" lt.macchina.Macchina eseguibile
```