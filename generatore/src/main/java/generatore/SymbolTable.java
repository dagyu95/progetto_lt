package generatore;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Stream;

public class SymbolTable implements Iterable<Descriptor> {

  private TreeMap<String,Descriptor> table; //implementazione basata su Vector

  /* Costruisce tabella vuota */
  public SymbolTable() {
	  table = new TreeMap<String,Descriptor>(Comparator.comparing(String::toString));
  }

  public void addDescriptor(Descriptor d) {
    table.putIfAbsent(d.getIdName(), d);
  }
  
  public Optional<Descriptor> getDescriptor(String completeName){
	    return Optional.ofNullable(table.get(completeName));
	}
  
  public TreeMap<String, Descriptor> getTable() {
	  return table;
  }
	
  @Override
  public Iterator<Descriptor> iterator() {
	  return table.values().iterator();
  }
  
  public Stream<Descriptor> stream() {
	  return table.values().parallelStream();
  }
  
  public String toString() {
	  StringBuilder sb = new StringBuilder();
	  sb.append("INIZIO ELENCO\n");
      table.values().stream().forEach(s -> sb.append(s).append("\n"));
      sb.append("FINE ELENCO\n");
      return sb.toString();
  }
}