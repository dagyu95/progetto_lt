package generatore;

public class Function extends Descriptor {

	public Function(String id) {
		super(id);
	}
	
	public boolean isMain() {
		return super.id.equalsIgnoreCase("main");
	}

}
