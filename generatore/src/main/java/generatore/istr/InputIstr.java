package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.INPUT;

public class InputIstr extends ExprIstr {

	@Override
	public void generaCodice(Codice c) {
		c.genera(INPUT);
	}

}
