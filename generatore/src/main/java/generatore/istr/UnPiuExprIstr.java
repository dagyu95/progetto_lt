package generatore.istr;

import lt.macchina.Codice;

public class UnPiuExprIstr extends ExprIstr {

	private ExprIstr e;
	
	public UnPiuExprIstr(ExprIstr e) {
		this.e = e;
	}
	
	public void generaCodice(Codice c) {
		e.generaCodice(c);
	}
	
	public String toString() {
		return e.toString() + "+";
	}

}
