package generatore.istr;

import static lt.macchina.Macchina.PUSHIMM;

import lt.macchina.Codice;

public class NumExprIstr extends ExprIstr {
	private Integer num;
	
	public NumExprIstr(Integer num) {
		this.num = num;
	}
	
	public void generaCodice(Codice c) {
		c.genera(PUSHIMM, num.intValue());
	}
	
	public String toString() {
		return num.toString();
	}
}
