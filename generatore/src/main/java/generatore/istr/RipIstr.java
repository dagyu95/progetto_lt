package generatore.istr;

import lt.macchina.Codice;

public abstract class RipIstr extends Istr {
	public abstract void generaCodice(Codice c);
}
