package generatore.istr;

import generatore.Descriptor;
import lt.macchina.Codice;

public class ModExprIstr extends ExprIstr {
	
	private ExprIstr e1;
	private ExprIstr e2;
	private Descriptor d1;
	private Descriptor d2;

	public ModExprIstr(ExprIstr e1, ExprIstr e2, Descriptor d1, Descriptor d2) {
		this.e1 = e1;
		this.e2 = e2;
		this.d1 = d1;
		this.d2 = d2;
	}

	@Override
	public void generaCodice(Codice c) {
		IdExprIstr t0 = new IdExprIstr(d1);
		IdExprIstr t1 = new IdExprIstr(d2);
		//calcolo e1
		new AssignIstr(t0,e1,false).generaCodice(c);
		new AssignIstr(t1,e2,false).generaCodice(c);
		//contiene e1-(e2*(e1/e2))
		new MenoExprIstr(t0,new PerExprIstr(t1,new DivExprIstr(t0,t1))).generaCodice(c);;
	}

}
