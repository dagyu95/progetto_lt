package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.JNZERO;

public class NeExprIstr extends ExprIstr {

	private ExprIstr e1;
	private ExprIstr e2;

	public NeExprIstr(ExprIstr e1, ExprIstr e2) {
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public void generaCodice(Codice c) {
		ConditionalJump.operators(JNZERO,e1,e2,c);
	}

}
