package generatore.istr;

import static lt.macchina.Macchina.POPIND;
import static lt.macchina.Macchina.ADD;

import lt.macchina.Codice;

public class AssignArrayIstr extends ExprIstr {

	private IdExprIstr id;
	private ExprIstr e1;
	private ExprIstr e2;
	private boolean is_expr;

	public AssignArrayIstr(IdExprIstr id, ExprIstr e1, ExprIstr e2,boolean is_expr) {
		this.id = id;
		this.e1 = e1;
		this.e2 = e2;
		this.is_expr=is_expr;
	}

	@Override
	public void generaCodice(Codice c) {
		id.generaIndirizzo(c);
		e1.generaCodice(c);
		c.genera(ADD);
		e2.generaCodice(c);
		c.genera(POPIND);
		if(is_expr) {
			//rilascia sullo stack il contenuto attuale dell'ident
			id.generaCodice(c);
		}
	}

}
