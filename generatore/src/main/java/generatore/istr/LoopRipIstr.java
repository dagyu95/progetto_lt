package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.JUMP;
import static lt.macchina.Macchina.JZERO;

public class LoopRipIstr extends RipIstr {

	private SeqIstr loopBody;
	private ExprIstr cond;

	public LoopRipIstr(ExprIstr cond, SeqIstr loopBody) {
		this.cond = cond;
		this.loopBody = loopBody;
	}

	@Override
	public void generaCodice(Codice c) {
		int inizio = c.indirizzoProssimaIstruzione();
		cond.generaCodice(c);
		int posizione = c.generaParziale(JZERO);
		loopBody.generaCodice(c);
		c.genera(JUMP,inizio);
		int fine = c.indirizzoProssimaIstruzione();
		c.completaIstruzione(posizione, fine);
	}

}
