package generatore.istr;

import lt.macchina.Codice;

public abstract class WriteIstr extends Istr {
	 public abstract void generaCodice(Codice c);
}
