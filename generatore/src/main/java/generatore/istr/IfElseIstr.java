package generatore.istr;

import static lt.macchina.Macchina.JUMP;
import static lt.macchina.Macchina.JZERO;

import lt.macchina.Codice;

public class IfElseIstr extends Istr{

	private ExprIstr cond;
	private SeqIstr s1;
	private SeqIstr s2;

	public IfElseIstr(ExprIstr cond, SeqIstr s1, SeqIstr s2) {
		this.cond = cond;
		this.s1 = s1;
		this.s2 = s2;
	}

	@Override
	public void generaCodice(Codice c) {
		cond.generaCodice(c);
		int is_false = c.generaParziale(JZERO);
		//esegui ramo if
		s1.generaCodice(c);
		int go_end = c.generaParziale(JUMP);
		c.completaIstruzione(is_false, c.indirizzoProssimaIstruzione());
		//esegui ramo else
		s2.generaCodice(c);
		c.completaIstruzione(go_end, c.indirizzoProssimaIstruzione());
	}

}
