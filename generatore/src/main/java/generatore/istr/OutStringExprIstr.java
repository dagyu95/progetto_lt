package generatore.istr;

import generatore.Descriptor;
import lt.macchina.Codice;

public class OutStringExprIstr extends WriteIstr {

	private StringIstr string;
	private Descriptor reg;
	private ExprIstr istr;

	public OutStringExprIstr(StringIstr string, ExprIstr istr, Descriptor reg) {
		this.string = string;
		this.istr = istr;
		this.reg = reg;
	}

	@Override
	public void generaCodice(Codice c) {
		new OutIstr(string,reg).generaCodice(c);
		new OutExprIstr(istr).generaCodice(c);
	}

}
