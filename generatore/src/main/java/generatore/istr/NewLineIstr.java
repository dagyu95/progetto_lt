package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.OUTPUTCH;
import static lt.macchina.Macchina.PUSHIMM;

public class NewLineIstr extends WriteIstr {

	@Override
	public void generaCodice(Codice c) {
		c.genera(PUSHIMM,10);
		c.genera(OUTPUTCH);
	}

}
