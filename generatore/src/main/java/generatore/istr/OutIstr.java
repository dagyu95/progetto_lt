package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.*;

import generatore.Descriptor;


public class OutIstr extends WriteIstr {

	private StringIstr string;
	private Descriptor reg;

	public OutIstr(StringIstr string,Descriptor reg) {
		this.string = string;
		this.reg = reg;
	}

	@Override
	public void generaCodice(Codice c) {
		string.generaCodice(c);
		int inizio = c.indirizzoProssimaIstruzione();
		IdExprIstr t0 = new IdExprIstr(reg);
		t0.storeInAddress(c);
		t0.generaCodice(c);
		t0.generaCodice(c);
		int posizione = c.generaParziale(JZERO);
		c.genera(OUTPUTCH);
		c.genera(JUMP,inizio);
		int fine = c.indirizzoProssimaIstruzione();
		c.completaIstruzione(posizione, fine);
		//rimuove lo 0 duplicato per evitare side effects
		c.genera(POPIMM);
	}

}
