package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.OUTPUT;;

public class OutExprIstr extends WriteIstr {

	private ExprIstr istr;

	public OutExprIstr(ExprIstr istr) {
		this.istr = istr;
	}

	@Override
	public void generaCodice(Codice c) {
		istr.generaCodice(c);
		c.genera(OUTPUT);
	}

}
