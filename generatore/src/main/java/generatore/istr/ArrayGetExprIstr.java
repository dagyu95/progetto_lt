package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.ADD;
import static lt.macchina.Macchina.PUSHIND;

public class ArrayGetExprIstr extends ExprIstr {
	
	private IdExprIstr id;
	private ExprIstr i;

	public ArrayGetExprIstr(IdExprIstr id, ExprIstr i) {
		this.id=id;
		this.i =i;
	}

	@Override
	public void generaCodice(Codice c) {
		id.generaIndirizzo(c);
		i.generaCodice(c);
		c.genera(ADD);
		c.genera(PUSHIND);
	}

}
