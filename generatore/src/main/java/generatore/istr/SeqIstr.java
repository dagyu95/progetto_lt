package generatore.istr;

import java.util.Iterator;
import java.util.Vector;
import java.util.stream.Stream;

import lt.macchina.Codice;

public class SeqIstr extends Istr implements Iterable<Istr>  {
	
	protected Vector<Istr> list;


	public SeqIstr() {
		list = new Vector<Istr>();
	}
	
	public void add(Istr i) {
		list.add(i);
	}
	
	public Vector<Istr> getList(){
		return list;
	}
	
	public void generaCodice(Codice c) {
		list.stream().forEach(e -> e.generaCodice(c));
	}
	
	@Override
	public Iterator<Istr> iterator() {
		return list.iterator();
	}
	
	public Stream<Istr> stream(){
		return list.stream();
	}

	public String toString() {
		return String.format("DIM: %d\n",list.size());
	}
	
}
