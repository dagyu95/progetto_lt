package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.JZERO;

public class EqExprIstr extends ExprIstr {

	private ExprIstr e1;
	private ExprIstr e2;

	public EqExprIstr(ExprIstr e1, ExprIstr e2) {
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public void generaCodice(Codice c) {
		ConditionalJump.operators(JZERO,e1,e2,c);
	}

}
