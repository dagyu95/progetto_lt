package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.POPIND;

public class AssignIstr extends ExprIstr {

	private IdExprIstr id;
	private ExprIstr e;
	private boolean is_expr;

	public AssignIstr(IdExprIstr id, ExprIstr e, boolean is_expr) {
		this.id = id;
		this.e = e;
		this.is_expr =is_expr;
	}

	@Override
	public void generaCodice(Codice c) {
		id.generaIndirizzo(c);
		e.generaCodice(c);
		c.genera(POPIND);
		if(is_expr) {
			//rilascia sullo stack il contenuto attuale dell'ident
			id.generaCodice(c);
		}
	}

}
