package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.*;

public class CharIstr extends Istr {
	
	private int ch;

	public CharIstr(int ch) {
		this.ch = ch;
	}

	@Override
	public void generaCodice(Codice c) {
		c.genera(PUSHIMM,ch);
	}

}
