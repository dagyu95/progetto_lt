package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.*;

public class StringIstr extends Istr {

	private String string;

	public StringIstr(String string) {
		this.string = string;
	}

	@Override
	public void generaCodice(Codice c) {
		c.genera(PUSHIMM, 0);
		String ns = new StringBuilder(string).reverse().toString();
		ns.chars().mapToObj(CharIstr::new).forEach(e -> e.generaCodice(c));
	}

}
