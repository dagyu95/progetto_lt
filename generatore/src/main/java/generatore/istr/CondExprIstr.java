package generatore.istr;

import static lt.macchina.Macchina.JZERO;
import static lt.macchina.Macchina.JUMP;

import lt.macchina.Codice;

public class CondExprIstr extends ExprIstr {

	private ExprIstr cond;
	private ExprIstr e1;
	private ExprIstr e2;


	public CondExprIstr(ExprIstr cond, ExprIstr e1, ExprIstr e2) {
		this.cond = cond;
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public void generaCodice(Codice c) {
		cond.generaCodice(c);
		int is_false = c.generaParziale(JZERO);
		//esegui ramo if
		e1.generaCodice(c);
		int go_end = c.generaParziale(JUMP);
		c.completaIstruzione(is_false, c.indirizzoProssimaIstruzione());
		//esegui ramo else
		e2.generaCodice(c);
		c.completaIstruzione(go_end, c.indirizzoProssimaIstruzione());
	}

}
