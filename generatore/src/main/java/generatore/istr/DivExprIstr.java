package generatore.istr;

import static lt.macchina.Macchina.DIV;

import lt.macchina.Codice;

public class DivExprIstr extends ExprIstr {

	private ExprIstr sx, dx;

	public DivExprIstr(ExprIstr sx, ExprIstr dx) {
		this.sx = sx;
		this.dx = dx;
	}
	
	public void generaCodice(Codice c) {
		sx.generaCodice(c);
		dx.generaCodice(c);
		c.genera(DIV);
	}
	
	public String toString() {
		return sx.toString() + " " + dx.toString() + " /";
	}

}
