package generatore.istr;

import static lt.macchina.Macchina.JUMP;
import static lt.macchina.Macchina.PUSHIMM;

import lt.macchina.Codice;

public class ConditionalJump {

	public static void operators(int op, ExprIstr e1, ExprIstr e2, Codice c) {

		new MenoExprIstr(e1,e2).generaCodice(c);
		// se il numero è minore di zero allora la condizione è vera
		int to_false = c.generaParziale(op);
		c.genera(PUSHIMM, 0);
		int to_next = c.generaParziale(JUMP);
		int falso = c.indirizzoProssimaIstruzione();
		c.completaIstruzione(to_false, falso);
		c.genera(PUSHIMM,1);
		int next = c.indirizzoProssimaIstruzione();
		c.completaIstruzione(to_next, next);
	}

}
