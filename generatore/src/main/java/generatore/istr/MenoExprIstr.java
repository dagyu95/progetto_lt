package generatore.istr;

import static lt.macchina.Macchina.SUB;

import lt.macchina.Codice;

public class MenoExprIstr extends ExprIstr {
	private ExprIstr sx, dx;

	public MenoExprIstr(ExprIstr sx, ExprIstr dx) {
		this.sx = sx;
		this.dx = dx;
	}

	public void generaCodice(Codice c) {
		sx.generaCodice(c);
		dx.generaCodice(c);
		c.genera(SUB);
	}

	public String toString() {
		return sx.toString() + " " + dx.toString() + " -";
	}

}
