package generatore.istr;

import static lt.macchina.Macchina.PUSH;
import static lt.macchina.Macchina.PUSHIMM;
import static lt.macchina.Macchina.POP;

import generatore.Descriptor;
import lt.macchina.Codice;

public class IdExprIstr extends ExprIstr {
	private Descriptor d;
	
	public IdExprIstr(Descriptor d) {
		this.d = d;
	}
	
	@Override
	public void generaCodice(Codice c) {
		int to_do = c.generaParziale(PUSH);
		d.addPointer(to_do);
	}
	
	public void generaIndirizzo(Codice c) {
		int to_do = c.generaParziale(PUSHIMM);
		d.addPointer(to_do);
	}

	public void storeInAddress(Codice c) {
		int to_do = c.generaParziale(POP);
		d.addPointer(to_do);
	}

}
