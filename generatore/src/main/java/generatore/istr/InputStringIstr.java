package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.INPUT;

import generatore.Descriptor;

public class InputStringIstr extends ExprIstr {
	
	private StringIstr s;
	private Descriptor d;

	public InputStringIstr(StringIstr s, Descriptor d) {
		this.s = s;
		this.d = d;
	}

	@Override
	public void generaCodice(Codice c) {
		new OutIstr(s,d).generaCodice(c);
		c.genera(INPUT);
	}

}
