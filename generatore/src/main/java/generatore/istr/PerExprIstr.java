package generatore.istr;

import static lt.macchina.Macchina.MUL;

import lt.macchina.Codice;

public class PerExprIstr extends ExprIstr {
	private ExprIstr sx, dx;
	
	public PerExprIstr(ExprIstr sx, ExprIstr dx) {
		this.sx = sx;
		this.dx = dx;
	}
	
	public void generaCodice(Codice c) {
		sx.generaCodice(c);
		dx.generaCodice(c);
		c.genera(MUL);
	}
	
	public String toString() {
		return sx.toString() + " " + dx.toString() + " *";
	}
}
