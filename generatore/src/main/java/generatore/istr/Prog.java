package generatore.istr;

import generatore.SymbolTable;
import lt.macchina.Codice;

public class Prog extends Istr {
	
	private SeqIstr si;
	private SymbolTable st;

	public Prog(SeqIstr si, SymbolTable st) {
		this.si = si;
		this.st = st;
	}
	
	public SymbolTable getSymbolTable() {
		return st;
	}

	@Override
	public void generaCodice(Codice c) {
		si.generaCodice(c);
	}

}
