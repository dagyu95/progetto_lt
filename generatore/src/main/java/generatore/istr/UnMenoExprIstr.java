package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.PUSHIMM;
import static lt.macchina.Macchina.SUB;

public class UnMenoExprIstr extends ExprIstr{
	private ExprIstr e;
	
	public UnMenoExprIstr(ExprIstr e) {
		this.e = e;
	}
	
	public void generaCodice(Codice c) {
		c.genera(PUSHIMM, 0);
		e.generaCodice(c);
		c.genera(SUB);
	}
	
	public String toString() {
		return e.toString() + "-";
	}
}
