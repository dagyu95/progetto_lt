package generatore.istr;

import lt.macchina.Codice;
import static lt.macchina.Macchina.JGTZ;

public class GtExprIstr extends ExprIstr {

	private ExprIstr e1;
	private ExprIstr e2;

	public GtExprIstr(ExprIstr e1, ExprIstr e2) {
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public void generaCodice(Codice c) {
		ConditionalJump.operators(JGTZ,e1,e2,c);
	}

}
