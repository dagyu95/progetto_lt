package generatore.istr;

import static lt.macchina.Macchina.ADD;

import lt.macchina.Codice;

public class PiuExprIstr extends ExprIstr {
	private ExprIstr sx, dx;
	
	public PiuExprIstr(ExprIstr sx, ExprIstr dx) {
		this.sx = sx;
		this.dx = dx;
	}
	
	public void generaCodice(Codice c) {
		sx.generaCodice(c);
		dx.generaCodice(c);
		c.genera(ADD);
	}
	
	public String toString() {
		return sx.toString() + " " + dx.toString() + " +";
	}
}
