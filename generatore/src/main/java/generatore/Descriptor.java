package generatore;

import java.util.Vector;

import lt.macchina.Codice;

public abstract class Descriptor{

	  protected String id;
	  private String file;
	  private String scope;
	  private Location l;
	  protected int length;
	  private Vector<Integer> pointer_list;

	  public Descriptor(String id) {
	    this.id = id;
	    pointer_list = new Vector<Integer>();
	    this.l = new Location(-1,-1);
	    updateContext();
	  }

	  public void updateContext() {
		file = Context.getFileName();
		scope = Context.getFunction();
	  }

	  public Location calculateAddress(int address) {
	    l = new Location(address,address+length);
	    return getLocation();
	  }
	  
	  public Location getLocation() {
		  return l;
	  }
	  
	  public int getLength() {
		  return length;
	  }
	  
	  public void addPointer(int point) {
		  this.pointer_list.add(point);
	  }
	  
	  public void updatePointers(Codice c) {
		  pointer_list.parallelStream().forEach(e -> c.completaIstruzione(e, l.getAddress()));
	  }
	  
	  
	  public String getIdName() {
		  return String.format("%s.%s.%s", file,scope,id);
	  }

	  public boolean equals(Descriptor d) {
		 return d.id.equalsIgnoreCase(this.id) && d.scope.equals(this.scope) && d.file.equals(this.file);
	  }

	  public boolean equals(Object o) {
	    if (o instanceof Descriptor)
	      return equals((Descriptor) o);
	    else
	      return false;
	  }
	  
	  public String getId() {
		  return this.id;
	  }
	  
	  public String toString() {
		  return String.format("%s %s at %s",this.getClass().getSimpleName(),getIdName(), l.toString());
	  }

	public void updateLocation(Location l) {
		this.l = l;
	}
}
