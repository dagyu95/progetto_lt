package generatore;

import java.io.File;

public class Context {

	private static File file;
	private static String function = "GlobalFunction";


	public static void setFile(File file) {
		Context.file = file;
	}
	
	public static void setFunction(String function) {
		Context.function = function;
	}
	
	public static File getFile() {
		return file;
	}
	
	public static void resetFunction() {
		Context.function = "GlobalFunction";
	}
	
	public static String getFunction() {
		return function;
	}
	
	public static String getCompleteName(String id) {
		return String.format("%s.%s.%s",Context.getFileName(),Context.getFunction(),id);
	}
	
	public static String getCompleteName(String id,String file, String function) {
		return String.format("%s.%s.%s",file,function,id);
	}

	public static String getFileName() {
		return getFile().getName().replaceFirst("[.][^.]+$", "");
	}
}
