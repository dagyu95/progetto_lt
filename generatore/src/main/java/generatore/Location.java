package generatore;

public class Location {

	private int start;
	private int end;
	
	public Location(int start, int end) {
		this.start = start;
		this.end = end;
	}
	
	public int getNext() {
		return end;
	}
	
	public int getAddress() {
		return start;
	}
	
	public String toString() {
		return String.format("[%d..%d]", start,end-1);
	}

}
