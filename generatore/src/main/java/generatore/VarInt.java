package generatore;

public class VarInt extends Descriptor {

	public VarInt(String id) {
		super(id);
		super.length = 1;
	}

}
