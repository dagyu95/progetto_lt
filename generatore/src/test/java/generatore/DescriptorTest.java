//package generatore;
//
//
//import org.junit.Test;
//import static org.assertj.core.api.Assertions.*;
//
//public class DescriptorTest {
//
//	@Test
//	public void RegisterTest() {
//		String nome = "$t0";
//		Descriptor d = new Register(nome);
//		assertThat(d.toString()).isEqualTo(String.format("%s %s.%s.%s with length %d", "Register", "GlobalFile","GlobalFunction",nome,1));
//	}
//	
//	@Test
//	public void ArrayTest() {
//		String nome = "x";
//		Descriptor d = new Array(nome,10);
//		assertThat(d.toString()).isEqualTo(String.format("%s %s.%s.%s with length %d", "Array", "GlobalFile","GlobalFunction",nome,10));
//	}
//	
//	@Test
//	public void VarIntTest() {
//		String nome = "x";
//		Descriptor d = new VarInt(nome);
//		assertThat(d.toString()).isEqualTo(String.format("%s %s.%s.%s with length %d", "VarInt","GlobalFile","GlobalFunction",nome,1));
//	}
//	
//	@Test
//	public void EqualityTest() {
//		Descriptor d = new VarInt("x");
//		Descriptor c = new VarInt("x");
//		assertThat(d.equals(c)).isTrue();		
//	}
//	
//	@Test
//	public void SymbolSearchDescriptorTest() {
//		Descriptor d = new VarInt("x");
//		SymbolTable s = new SymbolTable();
//		s.addDescriptor(d);
//		Descriptor c = new VarInt("x");
//		assertThat(s.getDescriptor(c).isPresent()).isTrue();
//	}
//	
//}
