//package generatore;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.ArrayList;
//
//import org.junit.Test;
//
//import generatore.istr.*;
//
//import static org.assertj.core.api.Assertions.*;
//
//import lt.macchina.Codice;
//import static lt.macchina.Macchina.*;
//
//
//public class IstrTest {
//	
//	private String exec(String file) throws IOException { 
//		Process p;
//
//		p = Runtime.getRuntime().exec("java -classpath libs/ltMacchina.jar lt.macchina.Macchina "+file);
//		try {
//			p.waitFor();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//
//		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
//		StringBuilder sb = new StringBuilder();
//		String line = "";			
//		while ((line = reader.readLine())!= null) {
//			sb.append(line + "\n");
//		}
//		return sb.toString();
//	}
//	
//  @Test 
//  public void OutIstrTest() throws IOException{
//	String file = "src/resources/OutIstrTestOutput";
//	String expected = "prova\n";
//	Codice c = new Codice(file);
//	//uso registro 0
//	c.genera(PUSHIMM,0);
//	OutIstr si = new OutIstr(new StringIstr(expected),new Descrittore("$t0",0));
//	si.generaCodice(c);
//	c.genera(HALT);
//	c.fineCodice();
//	assertThat(exec(file)).isEqualTo(expected);
//  }
//  
//  @Test
//  public void ExprIstrTest() throws IOException {
//	String file = "src/resources/ExprIstrTestOutput";
//	String expected = "7\n";
//	Codice c = new Codice(file);
//	c.genera(PUSHIMM,3);
//	Descriptor d = new Descriptor("x",0);
//	ExprIstr ei = new PiuExprIstr(new NumExprIstr(4),new IdExprIstr(d));
//	ei.generaCodice(c);
//	c.genera(OUTPUT);
//	c.genera(HALT);
//	c.fineCodice();
//	assertThat(exec(file)).isEqualTo(expected);
//  }
//  
//  @Test
//  public void OutExprIstrTest() throws IOException {
//	String file = "src/resources/OutExprIstrTestOuput";
//	String expected = "12\n";
//	Codice c = new Codice(file);
//	c.genera(PUSHIMM,3);
//	Descriptor d = new Descriptor("x",0);
//	ExprIstr ei = new PerExprIstr(new NumExprIstr(4),new IdExprIstr(d));
//	OutExprIstr oei = new OutExprIstr(ei);
//	oei.generaCodice(c);
//	c.genera(HALT);
//	c.fineCodice();
//	assertThat(exec(file)).isEqualTo(expected);
//  }
//  
//  @Test  
//  public void AssignIstrTest() throws IOException {
//	String file = "src/resources/AssignOutIstrTestOutput";
//	String expected = "2\n";
//	Codice c = new Codice(file);
//	//per allineare la var x sta in pos 0
//	c.genera(PUSHIMM,0);
//	Descriptor d = new Descriptor("x",0);
//	ArrayList<Istr> list = new ArrayList<Istr>();
//	ExprIstr e = new DivExprIstr(new NumExprIstr(4),new NumExprIstr(2));
//	IdExprIstr id = new IdExprIstr(d);
//	//x=4-2
//	list.add(new AssignIstr(id,e));
//	list.add(new OutExprIstr(id));
//	list.stream().forEach(i->i.generaCodice(c));
//	c.genera(HALT);
//	c.fineCodice();
//	assertThat(exec(file)).isEqualTo(expected);
//  }
//  
//  @Test
//  public void SubIdExprIstrTest() throws IOException {
//		String file = "src/resources/SubIdExprIstrTestOutput";
//		String expected = "3\n";
//		Codice c = new Codice(file);
//		Descrittore n = new Descrittore("n",0);
//		//initialize x=3
//		c.genera(PUSHIMM,3);
//		Descrittore i = new Descrittore("i",1);
//		//initialize i=2
//		c.genera(PUSHIMM,2);
//		SeqIstr list = new SeqIstr();
//		//n=4
//		list.add(new AssignIstr(new IdExprIstr(n),new NumExprIstr(4)));
//		//i=1
//		list.add(new AssignIstr(new IdExprIstr(i),new NumExprIstr(1)));
//		ExprIstr e = new MenoExprIstr(new IdExprIstr(n),new IdExprIstr(i));
//		list.add(new OutExprIstr(e));
//		list.add(new NewLineIstr());
//		list.generaCodice(c);
//		c.genera(HALT);
//		c.fineCodice();
//		assertThat(exec(file)).isEqualTo(expected);
//	  
//  }
//  
//  @Test
//  public void LoopIstrTest() throws IOException {
//		String file = "src/resources/LoopIstrTestOutput";
//		String expected = "1\n2\n3\n";
//		Codice c = new Codice(file);
//		Descriptor n = new Descriptor("n",0);
//		Descriptor i = new Descriptor("i",1);
//		c.genera(PUSHIMM,2);
//		c.genera(MOVESP);
//		SeqIstr list = new SeqIstr();
//		//n=4
//		list.add(new AssignIstr(new IdExprIstr(n),new NumExprIstr(4)));
//		//i=1
//		list.add(new AssignIstr(new IdExprIstr(i),new NumExprIstr(1)));
//		SeqIstr loopBody = new SeqIstr();
//		loopBody.add(new OutExprIstr(new IdExprIstr(i)));
//		loopBody.add(new NewLineIstr());
//		//i=i+1
//		ExprIstr sum = new PiuExprIstr(new IdExprIstr(i),new NumExprIstr(1));
//		loopBody.add(new AssignIstr(new IdExprIstr(i),sum));
//		//n-i
//		ExprIstr cond = new MenoExprIstr(new IdExprIstr(n),new IdExprIstr(i));
//		list.add(new LoopRipIstr(cond,loopBody));
//		list.generaCodice(c);
//		c.genera(HALT);
//		c.fineCodice();
//		assertThat(exec(file)).isEqualTo(expected);
//  }
//  
//}
//
//











